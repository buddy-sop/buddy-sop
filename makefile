
#Set OPTIMIZER to override the optimization level (defaults to -O2)
#Set INCDIRS to specify any additional include directories.
#Set LIBDIRS to specify any addition library include directories
#Set SOURCES to list all .C files required to build the DSO or App
#Set DSONAME to the name of the desired output DSO file (if applicable)
#Set APPNAME to the name of the desires output application (if applicable)

DSONAME=SOP_Buddy
SOURCES=SOP_Buddy.cpp
ICON=SOP_Buddy.png
HELP=Buddy.txt
 
HOUDINI_LOC = ~/houdini${HOUDINI_MAJOR_RELEASE}.${HOUDINI_MINOR_RELEASE}

LOCATION=${HOUDINI_LOC}/dso/
ICONLOCATION=${HOUDINI_LOC}/config/Icons/
HELPLOCATION=${HOUDINI_LOC}/help/nodes/sop/




UVAR := $(shell uname)

ifeq ($(OS), Windows_NT)
	ifndef OPTIMIZER
		OPTIMIZER = -Ox
	endif
	include ${HFS}/toolkit/makefiles/Makefile.win
	DSOEXT=.dll
	

else
	ifeq ($(UVAR), Darwin)
		include ${HFS}/toolkit/makefiles/Makefile.osx
		DSOEXT=.dylib

	else
		include ${HFS}/toolkit/makefiles/Makefile.linux
		DSOEXT=.so

	endif
endif

DSONAME:=$(DSONAME)$(DSOEXT)


#OPTIMIZER = -g -DUT_ASSERT_LEVEL=2
OPTIMIZER = -O2

OBJECTS = $(SOURCES:.cpp=.o)

ifdef DSONAME
TAGINFO = $(shell (echo -n "Compiled on:" `date`"\n         by:" `whoami`@`hostname`"\n$(SESI_TAGINFO)") | sesitag -m)

%.o:		%.cpp
	$(CC) $(OBJFLAGS)  -DMAKING_DSO $(TAGINFO) $< $(OBJOUTPUT) $@

$(DSONAME):	$(OBJECTS)
	$(LINK) $(SHAREDFLAG) $(OBJECTS)  $(DSOFLAGS) $(DSOOUTPUT) $@

else
%.o:		%.cpp
	$(CC) $(OBJFLAGS)  $< $(OBJOUTPUT) $@

$(APPNAME):	$(OBJECTS)
	$(LINK) $(OBJECTS) $(SAFLAGS) $(SAOUTPUT) $@
endif
	
install: $(DSONAME)
	 cp ${DSONAME} ${LOCATION}
	 cp ${ICON} ${ICONLOCATION}
	 cp ${HELP} ${HELPLOCATION}

default:	$(DSONAME) $(APPNAME)



clean:
	rm -f $(OBJECTS) $(APPNAME) $(DSONAME)
