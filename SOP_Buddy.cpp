/*
 * SOP_Buddy.h
 *
 *
 *    Copyright (c) 2013-2014 Sebastian H. Schmidt (sebastian<dot>h<dot>schmidt<at>googlemail<dot>com)
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.

 */

//! @todo:  - test locking when checking if a buddy is in use @Tree-Callback
//!         - add removing of buddyes when the buddy id is already in use
//!         - implement keep buddy parameter


#include "SOP_Buddy.h"

#include <PRM/PRM_Default.h>
#include <PRM/PRM_Name.h>
#include <OP/OP_OperatorTable.h>
#include <OP/OP_AutoLockInputs.h>

#include <UT/UT_WorkArgs.h>
#include <GA/GA_ElementGroupTable.h>
#include <GU/GU_Detail.h>
#include <UT/UT_PtrArray.h>
#include <GA/GA_Group.h>
#include <UT/UT_ParallelUtil.h>
#include <UT/UT_LockUtil.h>
#include <OpenEXR/IlmThreadMutex.h>

#define SHS_PARMDEF(name,prmName,prmLabel,numericDef,stringDef,help) \
    static PRM_Name     name##Name(prmName,prmLabel);\
    static PRM_Default  name##Default = PRM_Default(numericDef,stringDef);\
    static const char*  name##Help = help;



static PRM_Default	entityDefault(1);



SHS_PARMDEF(srcgrp,"srcgrp","Source Group",0,"","Specify a point group in the buddy-input geo")

SHS_PARMDEF(buddyattr,"buddyattr","Buddy Attribute",0,"tnum","The name of the attribute the 'id' of the buddy point gets stored in")
SHS_PARMDEF(buddygrp,"buddygrp","Buddy Group",0,"","Specify a point group in the buddy-input geo")

SHS_PARMDEF(buddyid,"buddyidattr","Buddy ID Attribute",0,"id","the attribute of the 'buddy' which should be used as its 'id' ")


SHS_PARMDEF(distance,"maxdist","Maximum Distance",0,"","Specify the max search radius, < 0 = infinite")

SHS_PARMDEF(keepbuddy,"keepbuddy","Keep Buddies",1,"","if the buddy-attribute value >= 0 for a point he wont get a new buddy")



SHS_PARMDEF(storevec,"storevec","Store Vector to Buddy",1,"","Generate and store the vector which points to the buddy")
SHS_PARMDEF(vecattr,"vecattr","Attribute Name",0,"N","the name of the attribute to store the vector in")



SHS_PARMDEF(sep1,"sep1","sep1",1,"","")
SHS_PARMDEF(sep2,"sep2","sep1",1,"","")


PRM_Template
SOP_Buddy::myTemplateList[] = {

    //PRM_Template(PRM_INT_J		,1	,&PRMentityName			,&entityDefault  ,&PRMentityMenuPointsAndPrimitives),
    PRM_Template(PRM_STRING     ,1  ,&srcgrpName           ,&srcgrpDefault,&SOP_Node::pointGroupMenu,NULL,NULL,NULL,NULL,srcgrpHelp),
    PRM_Template(PRM_STRING     ,1  ,&buddygrpName         ,&buddygrpDefault,&SOP_Node::pointGroupMenu,NULL,NULL,&SOP_Node::theSecondInput,NULL,buddygrpHelp),


    PRM_Template(PRM_FLT        ,1  ,&distanceName, &distanceDefault,0,0,0,0,0,distanceHelp),

    PRM_Template(PRM_STRING     ,1  ,&buddyattrName         ,&buddyattrDefault,&SOP_Node::pointAttribMenu,NULL,NULL,NULL,NULL,buddyattrHelp),
    PRM_Template(PRM_STRING     ,1  ,&buddyidName         ,&buddyidDefault,&SOP_Node::pointAttribMenu,NULL,NULL,&SOP_Node::theSecondInput,NULL,buddyidHelp),
    PRM_Template(PRM_TOGGLE     ,1  ,&keepbuddyName         ,&keepbuddyDefault,NULL,NULL,NULL,NULL,NULL,keepbuddyHelp),

    PRM_Template(PRM_SEPARATOR     ,1  ,&sep2Name),
    PRM_Template(PRM_TOGGLE     ,1  ,&storevecName         ,&storevecDefault,NULL,NULL,NULL,NULL,NULL,storevecHelp),
    PRM_Template(PRM_STRING     ,1  ,&vecattrName         ,&vecattrDefault,NULL,NULL,NULL,NULL,NULL,vecattrHelp),


    PRM_Template(),
};


typedef std::map<GA_Offset,size_t> tOffsetMap;

void
newSopOperator(OP_OperatorTable *table)
{
     table->addOperator(new OP_Operator("Buddy",
                                        "Buddy",
                                        SOP_Buddy::myConstructor,
                                        SOP_Buddy::myTemplateList,
                                         2,
                                         2,
                                         0));
}



OP_Node *
SOP_Buddy::myConstructor(OP_Network *net, const char *name, OP_Operator *op)
{
    return new SOP_Buddy(net, name, op);
}

const char *
SOP_Buddy::inputLabel(unsigned idx ) const
{
    if (idx==1)
    {
        return "Source Geometry";
    }
    else if (idx==2)
    {
        return "Buddy Geometry";
    }
    else
    {
        return "Unknown";
    }

}


SOP_Buddy::SOP_Buddy(OP_Network *net, const char *name, OP_Operator *op)   : SOP_Node(net, name, op),
    m_srcGrp(NULL),m_bdyGrp(NULL),m_bdyGdp(NULL),m_pData(NULL),m_tData(NULL),m_tree(NULL)
{

}

SOP_Buddy::~SOP_Buddy()
{

}


bool
SOP_Buddy::updateParmsFlags()
{
    bool    changed;

    changed  = enableParm(vecattrName.getToken(), getStoreDebugVec());

    return changed;
}

OP_ERROR
SOP_Buddy::cookInputGroups(OP_Context &context, int alone)
{
    // The SOP_Node::cookInputPointGroups() provides a good default
    // implementation for just handling a point selection.



    if (alone)
    {
        if (lockInputs(context) >= UT_ERROR_ABORT)
            return error();
    }

    m_srcGrp = NULL;
    m_bdyGrp = NULL;

    const GA_PointGroup* grp = NULL;
    const GA_PointGroup* bgrp = NULL;

    GU_DetailGroupPair oPair;


    OP_ERROR res =  cookInputPointGroups(context, grp, oPair,alone, /*do_selection =*/  false, /*input_index =*/ 0, /*parm_index =*/ 0);


    if(!alone)
    {
        m_srcGrp = grp;
    }


    if (res >= UT_ERROR_ABORT)
    {
        if (alone)
        {
            unlockInputs();
        }
        return res;
    }



    GU_DetailGroupPair bPair;
    GU_Detail *pgdpb = (GU_Detail *) inputGeo(1, context);

    res =  cookInputPointGroups(context, bgrp, bPair,1, /*do_selection =*/  false, /*input_index =*/ 1, /*parm_index =*/ 1,
                                /* group_type_index = */ -1,
                                /* allow_reference = */ true,
                                /* fetchgdp = */  false,
                                /* pgdp  =*/ pgdpb);


    if (bgrp != NULL)
    {
        m_bdyGrp = bgrp;
    }

    if (alone)
    {
        unlockInputs();
    }

    return res;

}




struct treeData
{
    GA_Offset offset;
    bool found;
    UT_Vector3 pos;
    UT_Lock *lock;

    treeData()
    {
        lock = NULL;
    }

    ~treeData()
    {
        if (lock != NULL)
        {
            delete lock;
            lock = NULL;
        }
    }



};


class op_fillTree {
public:
    op_fillTree ( UT_Vector3Array *pos,UT_Array<void *> *data, const GU_Detail *gdp,const tOffsetMap *offsetMap)
        :myPos(pos),myData(data),myGdp(gdp),myOffsetMap(offsetMap)
    {}
    void    operator()(const GA_SplittableRange &r) const
            {

                for (GA_Iterator it(r.begin()); !it.atEnd(); ++it)
                {

                    GA_Offset ptOff = it.getOffset();

                    size_t idx = myOffsetMap->at(ptOff);



                    UT_Vector3 pos = myGdp->getPos3(*it);

                    treeData *tData = new treeData();
                    tData->found=false;
                    tData->offset = ptOff;
                    tData->pos = pos;
                    tData->lock = new UT_Lock(false);


                    (*myPos)(idx) = pos;
                    (*myData)(idx) = (void*)tData;

                }
            }
private:
     UT_Vector3Array *myPos;
     UT_Array<void *> *myData;
     const GU_Detail *myGdp;
     const tOffsetMap *myOffsetMap;

};


void
fillTree(const GA_Range &range,  UT_Vector3Array *pos,UT_Array<void *> *data,const GU_Detail *gdp, const tOffsetMap *offsetMap)
{

    UTparallelFor(GA_SplittableRange(range), op_fillTree(pos,data,gdp,offsetMap));
}



int checkFound(void *data, void *userdata)
{
    treeData *myData = static_cast<treeData*> (data);
    int ret_val = 0;


    if (myData->found == false)
    {
        ret_val = 1;
    }

    return ret_val;
}



class op_findBuddy{

public:

    op_findBuddy ( UT_PointTree *tree, const GA_ROAttributeRef srcAttr, const GA_WOAttributeRef dstAttr, const GU_Detail* gdp, const float dist, const bool storeNormal,bool keepBuddy, const GA_WOAttributeRef nrmAttr)
        :myTree(tree),mySrcAttr(srcAttr),myDstAttr(dstAttr),myGdp(gdp),myDist(dist),myStoreNormal(storeNormal), myKeepBuddy(keepBuddy), myNrmAttr(nrmAttr)
    {}

    void operator() (const GA_SplittableRange &r) const
    {
        GA_RWPageHandleI dstH(myDstAttr.getAttribute());
        GA_ROHandleI     srcH(mySrcAttr.getAttribute());
        GA_RWPageHandleV3 nrmH;
        if (myStoreNormal)
        {
            nrmH = GA_RWPageHandleV3(myNrmAttr.getAttribute());
        }

        // itterate over pages
        for (GA_PageIterator pit = r.beginPages(); !pit.atEnd(); ++pit)
        {
            GA_Offset       start, end;

            // iterate over the elements in the page.
            for (GA_Iterator it(pit.begin()); it.blockAdvance(start, end); )
            {
                // Perform any per-page setup required, then
                dstH.setPage(start);
                if (myStoreNormal) nrmH.setPage(start);

                // itterate over the points
                for (GA_Offset i = start; i < end; ++i)
                {
                    // get position
                    UT_Vector3 p = myGdp->getPos3(i);

                    // get buddy id
                    int buddyID = dstH.get(i);
                    // if we already have a buddy, continue
                    if ((buddyID >-1 ) &&( myKeepBuddy))
                    {
                        continue;
                    }

                    bool found = false;
                    void* userData = (void*) &p;
                    treeData* buddyData = NULL;
                    while (!found)
                    {

                        void* data = myTree->findNearest(p,myDist,checkFound,userData);
                        if (data==NULL)
                        {
                            // we did not realy find something
                            found = true;
                        }
                        else
                        {
                            // lets cast the data to the tree data
                            treeData* tData = static_cast<treeData*> (data);
                            {
                                // aquire a lock for that buddy
                                UT_AutoLockType<UT_Lock> lock(*tData->lock);
                                // has this buddy be found before
                                if (!tData->found)
                                {
                                    tData->found= true;
                                    found = true;
                                }
                                // lock goes out of scope
                            }
                            if (found)
                                buddyData = tData;
                        }
                    }

                    int id = -1;
                    UT_Vector3 dbgNorm = UT_Vector3(0,0,0);
                    // we have buddyData
                    if ((buddyData != NULL))
                    {
                        // get id from buddy
                        id = srcH.get(buddyData->offset);
                        if (myStoreNormal)
                        {
                            dbgNorm = buddyData->pos - p;
                        }

                    }
                    // store it
                    dstH.set(i,id);
                    if (myStoreNormal)
                    {
                        nrmH.set(i,dbgNorm);
                    }

                }
            }
        }

    }
private:
     UT_PointTree *myTree;
    const GA_ROAttributeRef mySrcAttr;
    const GA_WOAttributeRef myDstAttr;    
    const GU_Detail *myGdp;
    const float myDist;
    const bool myStoreNormal;
    const bool myKeepBuddy;
    const GA_WOAttributeRef myNrmAttr;

};


void findBuddy(const GA_Range &range,
                UT_PointTree *tree,
               const GA_ROAttributeRef srcAttr,
               const GA_WOAttributeRef dstAttr,
               GU_Detail *gdp,
               const float dist,
               const bool storeNormal,
               const bool keepBuddy,
               const GA_WOAttributeRef nrmAttr)
{
    UTparallelFor(GA_SplittableRange(range),op_findBuddy(tree,srcAttr,dstAttr,gdp,dist,storeNormal,keepBuddy,nrmAttr));
}




void SOP_Buddy::cleanUp()
{
    if (m_pData!=NULL)
    {
        m_pData->clear();
        delete m_pData;
        m_pData = NULL;
    }
    if (m_tData != NULL)
    {
        exint max = m_tData->entries();
        for (exint i=0;i<max;i++)
        {

            delete static_cast<treeData*>((*m_tData)(i));
        }
        m_tData->clear();
        delete m_tData;
        m_tData = NULL;
    }
    if (m_tree != NULL)
    {
        delete m_tree;
        m_tree = NULL;
    }
}


void fillOffsetMap(GA_Range *range,  tOffsetMap *offMap)
{
    size_t i=0;
    for (GA_Iterator it(range->begin()); !it.atEnd(); ++it)
    {
        GA_Offset ptOff = it.getOffset();
        offMap->insert(std::pair<GA_Offset,size_t>(ptOff,i));
        i++;
    }
}


bool SOP_Buddy::buildTree(UT_Interrupt *boss,bool useGrp)
{


    const GA_PointGroup *grp = NULL;
    // do we have a group ?
    if (useGrp)
    {
        grp = m_bdyGrp;
    }

    // get the range from the group
    GA_Range range   = m_bdyGdp->getPointRange(grp);

    // if the range is empty
    if (range.empty())
    {
        addMessage(SOP_ATTRIBUTE_INVALID,"No Points to use as buddies found");
        return false;
    }

    // how many pts do we have in the range
    GA_Size entries  = range.getEntries();

    // build the position & data arrays
    m_pData = new UT_Vector3Array(entries,entries);
    m_tData = new UT_Array<void *>(entries,entries);

    // build the offset map, that maps the 'range' to the offsets
    tOffsetMap offMap;
    fillOffsetMap(&range,&offMap);

    // jump out if the user wants us to stop
    if (boss->opInterrupt())
    {
        return false;
    }

    // fill the arrays neccesary for the tree
    fillTree(range,m_pData,m_tData,m_bdyGdp,&offMap);

    // lets build the tree
    m_tree = new UT_PointTree();
    m_tree->build(*m_tData,*m_pData);

    return true;
}




bool SOP_Buddy::findBuddies(bool useGrp)
{

    const GA_PointGroup *grp = NULL;
    if (useGrp)
    {
        grp = m_srcGrp;
    }

    GA_Range range = gdp->getPointRange(grp);





    if (range.empty())
    {
        addMessage(SOP_ATTRIBUTE_INVALID,"No Points to find buddies for given");
        return false;
    }

    findBuddy(range,m_tree,m_bdyIDRef,m_bdyRef,gdp,m_distance,m_storeDebugVec,m_keepBuddy,m_vecRef);

    return true;
}


OP_ERROR SOP_Buddy::cookMySop(OP_Context &context)
{

    OP_AutoLockInputs	autolock;

    if (autolock.lock(*this, context) >= UT_ERROR_ABORT) return error();

    UT_Interrupt*	 boss	 = UTgetInterrupt();

    fpreal time 		= context.getTime();
    m_distance          = getDistance(time);
    if (m_distance <= 0)
    {
        m_distance = std::numeric_limits<float>::max();
    }
    m_storeDebugVec     = getStoreDebugVec(time);
    m_keepBuddy         = getKeepBuddy(time);

    UT_String buddyIDAttrName = getBuddyIDAttr(time);
    UT_String buddyAttrName  = getBuddyAttr(time);

    UT_String vecAttrName = getVecAttr(time);

    this->duplicateSource(0,context);
    m_bdyGdp = inputGeo(1);


    if (cookInputGroups(context)  >= UT_ERROR_ABORT)
    {
        return error();
    }



    m_bdyIDRef =  m_bdyGdp->findIntTuple(GA_ATTRIB_POINT,buddyIDAttrName.buffer(),1,1);

    if (! m_bdyIDRef.isValid())
    {
        addError(SOP_ATTRIBUTE_INVALID,"Given Buddy-ID attribute does not exist as Integer-Point Attribute on Input-Geo 2");
        return error();
    }
    m_bdyRef =  gdp->findIntTuple(GA_ATTRIB_POINT,buddyAttrName.buffer(),1,1);
    if (! m_bdyRef.isValid())
    {
        m_bdyRef = gdp->addIntTuple(GA_ATTRIB_POINT,buddyAttrName.buffer(),1,GA_Defaults(-1));
    }
    if (!m_bdyRef.isValid())
    {
        addError(SOP_ATTRIBUTE_INVALID,"Unable to find/create Buddy  attribute on Geometry");
        return error();
    }


    if (m_storeDebugVec)
    {
        m_vecRef = gdp->findFloatTuple(GA_ATTRIB_POINT,vecAttrName,3,3);
        if (!m_vecRef.isValid())
        {
            m_vecRef = gdp->addFloatTuple(GA_ATTRIB_POINT,vecAttrName,3);
        }
        if  (!m_vecRef.isValid())
        {
            addError(SOP_ATTRIBUTE_INVALID,"Unable to find/create Vector attribute on Geometry");
            return error();
        }
    }



    boss->opStart("Building Tree");
    bool res = buildTree(boss);
    boss->opEnd();


    if (boss->opInterrupt())
    {
        cleanUp();
        return error();
    }

    if (!res)
    {        
        cleanUp();
        return error();
    }

    boss->opStart("Finding Buddies");
    res = findBuddies();
    boss->opEnd();

    if (!res)
    {
        cleanUp();
        return error();
    }

    cleanUp();
    return error();
}


bool SOP_Buddy::getKeepBuddy(float time )
{
    return evalInt(keepbuddyName.getToken(),0,time) == 1;
}


bool SOP_Buddy::getStoreDebugVec(float time)
{
    return evalInt(storevecName.getToken(),0,time) == 1;
}

UT_String SOP_Buddy::getVecAttr(float time )
{
    UT_String ret_val;
    evalString(ret_val,vecattrName.getToken(),0,time);
    return ret_val;
}

UT_String SOP_Buddy::getBuddyIDAttr(float time )
{
    UT_String ret_val;
    evalString(ret_val,buddyidName.getToken(),0,time);
    return ret_val;
}

UT_String SOP_Buddy::getBuddyAttr(float time )
{
    UT_String ret_val;
    evalString(ret_val,buddyattrName.getToken(),0,time);
    return ret_val;
}


float SOP_Buddy::getDistance(float time)
{
    return evalFloat(distanceName.getToken(),0,time);
}
