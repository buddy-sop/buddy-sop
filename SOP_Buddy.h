
/*
 * SOP_Buddy.h
 *
 *
 *    Copyright (c) 2013-2014 Sebastian H. Schmidt (sebastian<dot>h<dot>schmidt<at>googlemail<dot>com)
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.

 */

#ifndef SOP_BUDDY_H
#define SOP_BUDDY_H


#include <UT/UT_DSOVersion.h>
#include <GU/GU_Detail.h>
#include <SOP/SOP_Node.h>
#include <UT/UT_PointTree.h>


class SOP_Buddy: public SOP_Node
{
public:
    static OP_Node  *myConstructor(OP_Network*,
                 const char *,
                 OP_Operator *);
    static PRM_Template myTemplateList[];

 protected:
                        SOP_Buddy(OP_Network *net, const char *name, OP_Operator *op);
    virtual 			~SOP_Buddy();

    virtual OP_ERROR  	cookMySop(OP_Context &context);


    virtual OP_ERROR  	cookInputGroups(OP_Context &context, int alone=0);

    //! @note: returns the label for the input
    //! @parm idx: input index
    virtual const char  *inputLabel(unsigned idx) const;


 private:

    //! @returns: the group-mask string
    UT_String getGroupString(float time=0);



    //! @returns: the distance within points should be fused
    float getDistance(float time=0.0f);

    //! @returns: name of the buddy id-attribute
    UT_String getBuddyIDAttr(float time );

    //! @returns: name of the buddy attribute
    UT_String getBuddyAttr(float time );

    //! @returns: if the debug vector should be stored
    bool getStoreDebugVec(float time = 0.0f);

    //! @returns: the attribute name where to store the vector to the buddy
    UT_String getVecAttr(float time );

    //! @returns: true if the user wants to keep buddy-ids that are already stored
    bool getKeepBuddy(float time );

    //! updates the parameter flags for enable/disable hide/unable
    bool updateParmsFlags();

  private:

    // builds the buddy-tree from the 2nd input geo
    bool buildTree(UT_Interrupt *boss,bool useGrp=true);

    // finds the buddies for the first input geo
    bool findBuddies(bool useGrp=true);

    // cleans up the data structures we keep around
    void cleanUp();
  private:

    // the tree for finding the buddies
    UT_PointTree *m_tree;

    // the distance we search for buddys
    float m_distance;
    // do we store the 'debug' vector which poitns to our buddy
    bool m_storeDebugVec;
    // do we want to keep the buddy that we already have
    bool m_keepBuddy;

    const GA_PointGroup *m_srcGrp,*m_bdyGrp;

    const GU_Detail *m_bdyGdp;

    UT_Vector3Array *m_pData;
    UT_Array<void *>     *m_tData;

    GA_ROAttributeRef m_bdyIDRef;
    GA_RWAttributeRef m_bdyRef,m_vecRef;


};

#endif // SOP_BUDDY_H
